<div class="form-group">
  {{ Form::label ('name', 'Nombre') }}
  {{ Form::text ('name', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
  {{ Form::label ('email', 'Correo electrónico') }}
  {{ Form::text ('email',null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
  @if ($user->exists)
    {{ Form::label('password', 'Actualizar contraseña') }}
  @else
    {{ Form::label('password', 'Contraseña') }}
  @endif
  {{ Form::password('password', array('class' => 'form-control')) }}
</div>

<div class="form-group">
  {{ Form::label ('usertype_id', 'Type de usuario') }}
  <!--array('0'=> '-- Selecciona --') + -->
  {{ Form::select('usertype_id', $usertypes, array('selected' => $user->usertype_id), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::submit('Guardar', array('class' => 'btn btn-success')) }}
 	  {{ link_to_route('users.index', 'Cancelar', null, array('class' => 'text-danger')) }}
</div>
<div>
    @if($user->id == 1)
      <div class="alert alert-warning">El usuario Administrador no prodra cambiar el tipo de usuario asignado por defecto</div>
    @endif
  </div>