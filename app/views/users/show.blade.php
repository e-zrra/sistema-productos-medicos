@extends('layouts.master')
@section('content')
    <h1> {{ $user->real_name }} </h1>
    <ul>
       <li> Nombre de usuario: {{ $user->real_name }} </li>
       <li> Email: {{ $user->email }} </li>
       <li> Nivel: {{ $user->level }} </li>
       <li> Activo: {{ ($user->active) ? 'Sí' : 'No' }} </li>
    </ul>
    <p> {{ link_to('users', 'Volver atrás') }} </p>
@stop