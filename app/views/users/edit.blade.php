@extends('layouts.master')
@section('content')
  
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif

 <div>
  {{ Form::model($user, array('method' => 'PUT' ,'route' => array('users.update', $user->id), 'id' => 'user')) }}
     @include('users._form')
  {{ Form::close() }}
  </div>
@stop