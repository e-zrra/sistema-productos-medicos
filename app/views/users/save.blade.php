@extends('layouts.master')
@section('content')
  
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif

 <div>
  {{ Form::model($user, array('route' => 'users.store')) }}
     @include('users._form')
     {{ Form::hidden('active', 1) }}
  {{ Form::close() }}
  </div>
@stop