@extends('layouts.master')
@section('content')

    @if(Session::has('notice'))
    	<div class="alert alert-success">  {{  Session::get('notice') }}
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        </div>
    @endif
    
    <div class="row">
        <div class="col-md-12">
            <h3 class="">
                Usuarios {{ link_to_route ('users.create', 'Nuevo', null, array('class' => 'btn btn-success')) }} 
            </h3>
        </div>
    </div>
        
    <div>
        <table class="table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Corre electrónico </th>
                    <th>Estatus </th>
                    <th>Tipo de usuario</th>
                    <th>Creado</th>
                    <th colspan="2">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td> {{ $user->name }}</td>
                    <td> {{ $user->email }} </td>
                    <td> {{ ($user->active) ? 'Activo' : 'Inactivo' }}</td>
                    <td> 
                    @if($user->type)
                        {{ $user->type->name }} 
                    @else
                        <i class="text-danger">No asignado</i>
                    @endif
                    </td>
                    <td> {{ $user->created_at }} </td>
                    <td> 
                        {{ link_to_route('users.edit', 'Editar', $user->id, array('class' => 'btn btn-primary')) }}
                    </td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('users.destroy', $user->id))) }}
                            {{ Form::submit('Eliminiar', array('class' => 'btn btn-danger destroy-user'))}}
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @if($users->count())
         {{ $users->links() }}
    @else
        <p class="text-danger">No se encontro registros</p>
    @endif
@stop
