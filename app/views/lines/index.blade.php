@extends('layouts.master')
@section('content')

@if(Session::has('notice'))
    <div class="alert alert-success">  {{  Session::get('notice') }}
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    </div>
@endif

<div class="row">
    <div class="col-md-12">
        <h3>
            Lineas {{ link_to_route ('lines.create', 'Nueva', null, array('class' => 'btn btn-success')) }} 
        </h3>
    </div>
</div>
<div>
    <table class="table table-condensed">
        <thead>
            <tr>
                <th> Id </th>
                <th> Nombre </th>
                <th> Descripción </th>
                <th> Dispositivo </th>
                <th colspan="2"> Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($lines as $line)
            <tr>
                <td> {{ $line->id }}</td>
                <td> {{ $line->name }} </td>
                <td> {{ $line->description }} </td>
                <td> {{ ($line->device) ? $line->device->name : 'No asignado' }}</td>
                <td> 
                    {{ link_to_route('lines.edit', 'Editar', $line->id, array('class' => 'btn btn-primary')) }}
                </td>
                <td>
                    {{ Form::open(array('method' => 'DELETE', 'route' => array('lines.destroy', $line->id))) }}
                        {{ Form::submit('Eliminiar', array('class' => 'btn btn-danger destroy-register'))}}
                    {{ Form::close() }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@if($lines->count())
    {{ $lines->links(); }}
@else
    <p class="text-danger">No se encontro registros</p>
@endif

@stop
