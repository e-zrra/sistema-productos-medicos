@extends('layouts.master')
@section('content')
  
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif

 <div>
  {{ Form::model($line, array('method' => 'PUT' ,'route' => array('lines.update', $line->id), 'id' => '')) }}
     @include('lines._form')
  {{ Form::close() }}
  </div>
@stop