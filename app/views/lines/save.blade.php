@extends('layouts.master')
@section('content')
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif

<div class="content-data" id="">
  {{ Form::open(array('url' => 'lines/' . $line->id)) }}
      @include('lines._form')
  {{ Form::close() }}
</div>
@stop