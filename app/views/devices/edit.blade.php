@extends('layouts.master')
@section('content')
  
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif

 <div>
  {{ Form::model($device, array('method' => 'PUT' ,'route' => array('devices.update', $device->id), 'id' => '')) }}
     @include('devices._form')
  {{ Form::close() }}
  </div>
@stop