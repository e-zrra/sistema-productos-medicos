<div class="form-group">
	{{ Form::label ('id', 'Id') }}
	{{ Form::text ('id', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label ('name', 'Nombre') }}
	{{ Form::text ('name', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label ('description', 'Description') }}
	{{ Form::textarea ('description', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
   	{{ Form::submit('Guardar', array('class' => 'btn btn-success')) }}
   	{{ link_to_route('devices.index', 'Cancelar', null, array('class' => 'text-danger')) }}
</div>