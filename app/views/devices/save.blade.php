@extends('layouts.master')
@section('content')

  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif
 <div class="content-data" id="">
  {{ Form::open(array('url' => 'devices/' . $device->id)) }}
    @include('devices._form')
  {{ Form::close() }}
</div>
@stop