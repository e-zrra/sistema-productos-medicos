@extends('layouts.master')
@section('content')

    @if(Session::has('notice'))
    	<div class="alert alert-success">  {{  Session::get('notice') }}
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        </div>
    @endif
    
    <div class="row">
        <div class="col-md-12">
            <h3 class="">
                Device {{ link_to_route ('devices.create', 'Nuevo', null, array('class' => 'btn btn-success')) }} 
            </h3>
        </div>
    </div>
        
    <div>
        <table class="table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Descripción </th>
                    <th>Creado</th>
                    <th colspan="2">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($devices as $device)
                <tr>
                    <td> {{ $device->name }}</td>
                    <td> {{ $device->description }} </td>
                    <td> {{ $device->created_at }} </td>
                    <td> 
                        {{ link_to_route('devices.edit', 'Editar', $device->id, array('class' => 'btn btn-primary')) }}
                    </td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('devices.destroy', $device->id))) }}
                            {{ Form::submit('Eliminiar', array('class' => 'btn btn-danger destroy'))}}
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @if($devices->count())
         {{ $devices->links() }}
    @else
        <p class="text-danger">No se encontro registros</p>
    @endif
@stop
