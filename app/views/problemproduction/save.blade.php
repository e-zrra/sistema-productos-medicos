@extends('layout.master')
@section('content')
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif

<div class="content-data" id="">
  <div class="panel-data-edit">
  {{ Form::open(array('url' => 'problemproduction/' . $pp->id)) }}
  	
      Select color:
      <br> 
      <input type="color" name="color" value="{{$pp->color_identify}}">
      <br>
      @if($pp->id)
          {{ Form::hidden ('_method', 'PUT') }}
      @endif
      {{ Form::label ('name', 'Name') }}
      <br />
      {{ Form::text('name', $pp->name, array('id'=>'name')) }}
      <br />

     	<br />
      {{ Form::label ('pp_description', 'Description') }}
      <br />
      {{ Form::textarea ('pp_description', $pp->pp_description) }}
      <br />

     	{{ Form::submit('Save') }}
     	{{ link_to('problemproduction', 'Cancel') }}
  {{ Form::close() }}
</div>
</div>
@stop