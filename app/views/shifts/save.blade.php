@extends('layout.master')
@section('content')
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif
<div class="content-data" id="">
  <div class="panel-data-edit">
  {{ Form::open(array('url' => 'shifts/' . $shift->id)) }}
  	
      Select color:
      <br> 
      <input type="color" name="color" value="{{$shift->color_identify}}">
      <br> 
      Start time:
      <br>
      <input type="time" name="start_time" value="{{$shift->start_time}}">
      <br> 
      Finish time:
      <br>
      <input type="time" name="finish_time" value="{{$shift->finish_time}}">

      @if($shift->id)
          {{ Form::hidden ('_method', 'PUT') }}
      @endif

     	<br />

      {{ Form::label ('shi_description', 'Description') }}
      <br />
      {{ Form::textarea ('shi_description', $shift->shi_description) }}
      <br />

     	{{ Form::submit('Save') }}
     	{{ link_to('shifts', 'Cancel') }}
  {{ Form::close() }}
</div>
</div>
@stop