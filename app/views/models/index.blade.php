@extends('layouts.master')
@section('content')

@if(Session::has('notice'))
    <div class="alert alert-success">  {{  Session::get('notice') }}
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    </div>
@endif
<div class="row">
    <div class="col-md-6">
        <h3 class="pull-left">Modelos
            {{ link_to_route('models.create', ' Nuevo', null, array('class' => 'btn btn-success')) }} 
        
        </h3>
    </div>
</div>
<div>
    <table class="table">
        <thead>
            <tr>
                <th> Modelo </th>
                <th> Descrición </th>
                <th> Modelo</th>
                <th> Cavidad</th>
                <th colspan="2"> Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($models as $model)
            <tr class="">
                <td>{{ $model->name }}</td>
                <td> {{ ($model->description) ? $model->description : 'Sin descripción' }} </td>
                <td>
                @if($model->mold)
                    {{ $model->mold->name }}
                @else
                    <i class="text-danger">Molde no asignado</i>
                @endif
                </td>
                <td>
                    @if($model->cavity)
                        {{ $model->cavity }}
                    @else
                        <i class="text-danger">No asignado</i>
                    @endif
                </td>
                <td> 
                    {{ link_to_route('models.edit', 'Editar', $model->id, array('class' => 'btn btn-primary')) }}
                </td>
                <td>
                    {{ Form::open(array('method' => 'DELETE', 'route' => array('models.destroy', $model->id))) }}
                        {{ Form::submit('Eliminiar', array('class' => 'btn btn-danger destroy-register'))}}
                    {{ Form::close() }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@if($models->count())
     {{ $models->links() }}
@else
    <p class="text-danger">No se encontro registros</p>
@endif
@stop
