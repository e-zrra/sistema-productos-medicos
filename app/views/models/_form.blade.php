<div class="form-group">
  {{ Form::label ('name', 'Nombre') }}
	  {{ Form::text ('name', null, array('class' => 'form-control')) }}
</div>
<div class="form-group">
  {{ Form::label ('description', 'Descripción') }}
  {{ Form::textarea ('description',null, array('class' => 'form-control')) }}
</div>
<div class="form-group">
  {{ Form::label ('cavity', 'Cavidad') }}
	  {{ Form::text ('cavity', null, array('class' => 'form-control')) }}
</div>
<div class="form-group">
  {{ Form::label ('mold_id', 'Molde') }}
  {{ Form::select('mold_id', array('0'=> '-- Selecciona --') + $molds, array('selected' => $model->mold_id), array('class' => 'form-control'))}}
</div>

<div class="form-group">
    {{ Form::submit('Guardar', array('class' => 'btn btn-success')) }}
	{{ link_to('models', 'Canceler', null, array('class' => 'text-danger')) }}
</div>