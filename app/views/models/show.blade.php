@extends('layouts.master')
@section('content')
	@if($model)
    	<h1> Modelo {{ $model->id }} </h1>
    
       	<span>Nombre: {{ $model->name }} </span><br />
       	@if($model->mold)
       		<span>Nombre de molde: {{ $model->mold->name }} </span>
    	@endif

    	<p> {{ link_to_route('models.edit', 'Editar', $model->id, array('class' => 'btn btn-primary')) }} 
    	{{ link_to('models', 'Volver atrás') }} </p>
    @endif
@stop