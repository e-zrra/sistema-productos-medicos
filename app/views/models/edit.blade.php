@extends('layouts.master')
@section('content')
  
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif

 <div>
  {{ Form::model($model, array('method' => 'PUT' ,'route' => array('models.update', $model->id), 'id' => '')) }}
    @include('models._form')
  {{ Form::close() }}
  </div>
@stop