@extends('layout.master')
{{ HTML::style('css/molds.css') }}
@section('content')

    
        <p><strong class="message-successful">  {{  Session::get('notice') }} {{ HTML::image('images/icos/message.png', 'Imagen not found', array('class'=>'imageIcos')) }}</strong></p>
    
    <p> 
        {{ HTML::image('images/icos/mold.png', ' Imagen not found' ,array('class'=>'imageIcos')) }}
        {{ link_to ('defects/create', ' Create new defect ') }} 
    </p>

    @if($defects->count())
        <div class="content-data" id="content-production-defects">
        <div class="title-content-data">List of Defects </div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th> Number </th>
                        <th> Name </th>
                        <th> Description </th>
                        <th> Problem Type  </th>
                        <!--<th> </th>-->
                        <th> </th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($defects as $item)
                    <tr>
                        <td> {{ $item->number }} </td>
                        <td> {{ $item->name }} </td>
                        <td> {{ $item->def_description }} </td>
                        <td> {{ $item->problemproduction->name }} </td>
                        @if($autoriz)
                            <td> 
                                <a href="defects/{{$item->id}}/edit">{{ HTML::image('images/icos/edit.png', 'Imagen not found', array('class'=>'imageIcos')) }}</a>
                                <a class="defect_delete">
                                    <input id="defect_id" type="hidden" value="{{$item->id}}" />
                                    <img src="images/icos/delete.png" class="imageIcos">
                                </a> 
                            </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
        <p>no se encontro datos</p>
    @endif

    @stop
