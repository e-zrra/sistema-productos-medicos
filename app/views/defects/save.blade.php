@extends('layout.master')
@section('content')
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif
<div class="content-data" id="">
  <div class="panel-data-edit">
  {{ Form::open(array('url' => 'defects/' . $defect->id)) }}

      {{ Form::label ('number', 'Number') }}
      <br />
      {{ Form::text ('number', $defect->number) }}
      <br />

  	

      @if($defect->id)
          {{ Form::hidden ('_method', 'PUT') }}
      @endif
      {{ Form::label ('defect', 'Name') }}
     	<br />
     	{{ Form::text ('name', $defect->name) }}
     	<br />

       <br />
      {{ Form::label ('problemproduction_id', 'Type production') }}
      <br />
      {{ Form::select('problemproduction_id', $pp, array('selected' => $defect->problemproduction_id))}}

      <br />
      {{ Form::label ('def_description', 'Description') }}
      <br />
      {{ Form::textarea ('def_description', $defect->def_description) }}
      <br />

     	{{ Form::submit('Save') }}
     	{{ link_to('defects', 'Cancel') }}
  {{ Form::close() }}
</div>
</div>
@stop