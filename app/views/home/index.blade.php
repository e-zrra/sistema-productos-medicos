@extends('layouts.master')
@section('content')

<div class="row">
	<div class="col-md-9">
		<h3>Producción del día de hoy</h3>
	</div>
</div>

<table class="table production-table">
    <thead>
        <th>
            Modelo
        </th>
        <th>
            Linea
        </th>
        <th>
            Plan
        </th>
        <th>
            Total del plan
        </th>
        <th width="180">
            Completo la producción
        </th>
        <th width="200">
            Total por cavidad / Inyecciones
        </th>
    </thead>
    <tbody>
        @foreach($productions as $production)
        <tr href="{{ route('production_history', 
            array(
                ($production->line) ? $production->line->id : $production->line_name, 
                $production->model_id,
                date('Y-m-d', strtotime($production->start_date) ),  
                date('Y-m-d', strtotime($production->end_date) ) ),
                null) }}">
            <td>
                @if($production->plan)
                    @if($production->plan->model)
                        <a href="{{ route('models.show', $production->plan->model->id) }}">
                            {{ $production->plan->model->name }}
                        </a>
                    @else
                        <i class="text-danger">Sin registro</i>
                    @endif
                @else
                    <i class="text-danger">Sin registro</i>
                @endif
            </td>
            <td><!-- LINEA -->
                @if($production->line)
                    <a href="{{ route('lines.show', $production->line->id) }}">{{ $production->line->name }}</a>
                @else
                    <i class="text-danger">Sin registro ({{ $production->line_id }})</i>
                @endif
            </td>
            <td> <!-- ID DE PLAN --> 
                @if($production->plan)
                    <a href="{{ route('plan.show', $production->plan->id) }}" class="label label-primary" style="font-size:14px;">{{ $production->plan->id }}</a>
                @else
                    <i class="text-danger">Sin registro</i>
                    @if($production->plan_id != 0)
                        <p>
                            <b>Plan antiguo</b> ({{ $production->plan_id }})
                        <p>
                    @endif
                @endif
            </td>
            <td>
                @if($production->plan)
                    <b class="label label-info" style="font-size:16px;">{{ $production->plan->quantity }}</b>
                @else
                    <i class="text-danger">Sin registro</i>
                @endif
            </td>
            <td>
                @if($production->plan)
                    <i class="glyphicon {{ finished_production($production, $production->quantity) }}"></i>
                @else
                    <i class="text-danger">Sin registro</i>
                @endif
            </td>

            <td>
                <div>
                    @if($production->model)
                        <span class="label label-success" style="font-size:16px;">
                            {{ get_total_production($production) }}
                        </span>
                    @else
                        0
                    @endif &nbsp; / &nbsp;
                    <span class="label label-warning" style="font-size:16px;">{{ $production->quantity }}</span>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@if(!$productions->count())
    <i>No se encontraron registros</i>
@endif

<?php

    $today = date('Y-m-d');

    $dates = array();

    $view_date = array();

    for ($i=0; $i <= 6 ; $i++) {

        $date = strtotime('+'.$i.' day', strtotime($today));

        $dates[$i] = date('Y-m-d', $date);

        $view_date[$i] = date('d D, M', $date);
    }

    $plan_model = Plan::where('production_date_begin', $today)->distinct()->get();
  ?>
<hr />

<table class="table">
    <thead>
        <tr>
            <th></th>
            <th colspan="8">Días</th>
        </tr>
        <tr>
             <th>Modelo</th>
             <th>Linea</th>
            @for($i = 0; $i < sizeof($view_date); $i++)
                <th style="background-color:#CCCCCC;">
                    {{ $view_date[$i] }}
                </th>
            @endfor
        </tr>
    </thead>
    <tbody>
        @foreach($lines as $line)
            <tr>
                <?php
                    $plans = Plan::where('line_id', $line->id)->where('production_date_begin', '>=', $today)
                    ->where('production_date_begin', '<=', $dates[6])
                    ->orderBy('production_time_begin', 'ASC')
                    ->orderBy('production_date_begin', 'ASC')
                    ->get();
                ?>
                @foreach($plans as $plan)
                    <tr>

                        @if($plan->model)
                        <td>
                            <a target="_blank" href="{{ route('plan.show', $plan->id) }}">
                            {{ $plan->model->name }}
                            </a>
                        </td>
                        <td>
                            <a target="_blank" href="{{ route('lines.show', $line->id) }}"> 
                                {{ $line->name }}
                            </a>
                        </td>
                        @else
                        <td colspan="2">
                            <i class="text-danger">Información removida</i>
                        </td>
                        @endif
                        
                        {{ day_production($plan, $dates) }}
                        
                    </tr>
                @endforeach
            </tr>
        @endforeach
        
    </tbody>
</table>
<div>
    <p>
        <span>Finalizado</span>
        <label class="label label-success">&nbsp;</label>
    </p>

    <p>
        <span>Cancelado</span>
        <label class="label label-danger">&nbsp;</label>
    </p>
    <!-- <p>
        <span>Cancelado</span>
        <label class="label label-danger">&nbsp;</label>
    </p> -->
</div>
@stop