@extends('layouts.master')
@section('content')
  
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif

 <div>
  {{ Form::model($mold, array('method' => 'PUT' ,'route' => array('molds.update', $mold->id), 'id' => '')) }}
    @include('molds._form')
  {{ Form::close() }}
  </div>
@stop