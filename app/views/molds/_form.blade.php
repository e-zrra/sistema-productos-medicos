<div class="form-group">
  {{ Form::label ('name', 'Nombre') }}
 	{{ Form::text ('name', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
  {{ Form::label ('description', 'Descripción') }}
  {{ Form::textarea ('description', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::submit('Guardar', array('class' => 'btn btn-success')) }}
    {{ link_to_route('molds.index', 'Cancelar', null, array('class' => 'text-danger')) }}
</div>