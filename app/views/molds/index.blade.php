@extends('layouts.master')
@section('content')

    @if(Session::has('notice'))
        <div class="alert alert-success">  {{  Session::get('notice') }}
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        </div>
    @endif

    <div class="row">
    
        <div class="col-md-6">
            <h3 class="pull-left">Moldes
                {{ link_to_route ('molds.create', 'Nuevo', null, array('class' => 'btn btn-success')) }} 
            </h3>
        </div>
        <div class="col-md-6">
            <p class="pull-right">
                </p>
        </div>
    </div>

    <div>
        <table class="table">
            <thead>
                <tr>
                    <th> Nombre </th>
                    <th> Descripción </th>
                    <th colspan="2"> Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($molds as $mold)
                <tr>
                    <td> {{ $mold->name }}</td>
                    <td> {{ $mold->description }} </td>
                    <td> 
                        {{ link_to_route('molds.edit', 'Editar', $mold->id, array('class' => 'btn btn-primary')) }}
                    </td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('molds.destroy', $mold->id))) }}
                            {{ Form::submit('Eliminiar', array('class' => 'btn btn-danger destroy-register'))}}
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
   @if($molds->count())
         {{ $molds->links() }}
    @else
        <p class="text-danger">No se encontro registros</p>
    @endif 
@stop
