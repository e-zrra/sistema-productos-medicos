@extends('layouts.master')
@section('content')

@if(isset($errors))
  <ul>
    @foreach($errors as $item)
      <li> {{ $item }} </li>
    @endforeach
  </ul>
@endif

 <div class="content-data">
  {{ Form::model($mold, array('route' => 'molds.store')) }}
    @include('molds._form')
  {{ Form::close() }}
</div>
@stop