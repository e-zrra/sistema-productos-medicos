<!DOCTYPE html>
<html>
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title> Productos medicos </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?= stylesheet_link_tag() ?>
 </head>
 <body>
 	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!--<a class="navbar-brand" href="#">Proyecto</a>-->
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><!--Request::segment(1) == '/'-->
              <a href="{{ route('home.index') }}" class="{{ (Request::is('/')) ? 'active' : '' }}">
                Inicio
              </a>
            </li>
            <li>
              <a href="{{ route('production.index') }}" class="{{ (Session::get('nav-option') == 'production') ? 'active' : '' }}">Producción</a>
            </li>
            <li>
              <a href="{{ route('users.index') }}" class="{{ (Session::get('nav-option') == 'users') ? 'active' : '' }}">Usuarios</a>
            </li>
            <li>
              <a href="{{ route('models.index') }}" class="{{ (Session::get('nav-option') == 'models') ? 'active' : '' }}">Modelos</a>
            </li>
            <li>
              <a href="{{ route('molds.index') }}" class="{{ (Session::get('nav-option') == 'molds') ? 'active' : '' }}">Moldes</a>
            </li>
            <li>
              <a href="{{ route('lines.index') }}" class="{{ (Session::get('nav-option') == 'lines') ? 'active' : '' }}">Lineas</a>
            </li>
            <li>
              <a href="{{ route('plan.index') }}" class="{{ (Session::get('nav-option') == 'plan') ? 'active' : '' }}">Plan</a>
            </li>
            <li>
              <a href="{{ route('devices.index') }}" class="{{ (Session::get('nav-option') == 'devices') ? 'active' : '' }}">Dispositivos</a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Configuración <span class="caret"></span></a>
              <!--<ul class="dropdown-menu" role="menu">
                <li><a href="{{ route('perfil') }}">Perfil</a></li>
                <li class="divider"></li>
                <li>
                  {{ link_to_route('login.logout', 'Cerrar sesión', null, array('class' => '')) }}
                </li>
              </ul>-->
            </li>
          </ul>
          <ul>
            <li>
              {{ Form::open(array('method' => 'GET', 'url' => Request::path(), 'class' => 'navbar-form navbar-right')) }}
                <input type="text" name="search" value="{{ (Input::get('search')) ? Input::get('search') : '' }}" class="form-control" placeholder="Buscar...">
                {{ Form::submit('Buscar', array('class' => 'btn btn-primary')) }}
              {{ Form::close() }}
            </li>
          </ul>
        </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
    </div><!-- /.navbar -->

    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-12">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="row">
            <div class="col-12">
              @yield('content')
            </div>
          </div><!--/row-->
        </div><!--/span-->
      </div><!--/row-->
    </div><!--/.container-->
    <div class="footer">
      <div class="container">
        <p class="text-muted">Company</p>
      </div>
    </div>
</body>
{{ javascript_include_tag() }}
</html>
