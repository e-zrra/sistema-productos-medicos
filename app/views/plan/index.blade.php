@extends('layouts.master')
@section('content')

@if(Session::has('notice'))
    <div class="alert alert-success">  {{  Session::get('notice') }} 
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    </div>
@endif
    
    <div class="row">
        <div class="col-md-6">
            <h3>Plan
                {{ link_to_route ('plan.create', 'Nuevo', null, array('class' => 'btn btn-success')) }} 
            </h3>
        </div>
    </div>

    <div>
        <table class="table">
            <thead>
                <tr>
                    <th> Linea </th>
                    <th> Modelo </th>
                    <th> Molde </th>
                    <th> F/H inicio</th>
                    <th> F/H finaliza</th>
                    <th> Cantidad</th>
                    <th> Estatus </th>
                    <th colspan="2">Acciones</th>
                </tr>
            </thead>
            <tbody id="data_plan">
                @foreach($plans as $plan)
                 <tr>
                    <td>
                        @if($plan->line)
                            <a href="{{ route('lines.show', $plan->line->id) }}">
                                {{ str_limit( ($plan->line->name) ? $plan->line->name : 'Sin nombre', 10, '...') }}
                            </a>
                        @else
                            <i class="text-danger">No asignado</i>
                        @endif
                    </td>
                    <td>
                    @if($plan->model)   
                        <a href="{{ route('models.show', $plan->model->id) }}">
                            {{ str_limit($plan->model->name, 10, '...') }}
                        </a>
                    @else
                        <i class="text-danger">No asignado</i>
                    @endif
                    </td>
                    <td>
                    @if($plan->model)
                        @if($plan->model->mold)
                            <a href="{{ route('molds.show', $plan->model->mold->id) }}">
                                {{ str_limit($plan->model->mold->name, 10, '...') }}
                            </a>
                        @endif
                    @else
                        <i class="text-danger">No asignado</i>
                    @endif
                    </td>
                    
                    <td> {{ $plan->production_date_begin }} <br /> {{ $plan->production_time_begin }} </td>

                    <td> {{ $plan->production_date_end  }} <br /> {{ $plan->production_time_end }} </td>
                    
                    <td> {{ $plan->quantity }} </td>

                    <td>
                        <span  class="label label-{{ ($plan->status_plan == 1) ? 'success' : 'danger' }}" >
                        @if($plan->status_plan == 1)
                            Activo
                        @elseif($plan->status_plan == 2)
                            Inactivo
                        @else
                            <i class="text-danger">Erroneo</i>
                        @endif
                        </span>
                    </td>

                    <td> 
                        {{ link_to_route('plan.edit', 'Editar', $plan->id, array('class' => 'btn btn-primary')) }}
                    </td>

                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('plan.destroy', $plan->id))) }}
                            {{ Form::submit('Eliminiar', array('class' => 'btn btn-danger destroy-register'))}}
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @if($plans->count())
         {{ $plans->links() }}
    @else
        <p class="text-danger">No se encontro registros</p>
    @endif
@stop
