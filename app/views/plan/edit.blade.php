@extends('layouts.master')
@section('content')
  
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif

 	<div class="row">

	<div class="col-md-8 col-md-offset-2">
		<h3>
			Editar
		</h3>
		<hr />
		  {{ Form::model($plan, array('method' => 'PUT' ,'route' => array('plan.update', $plan->id), 'id' => '')) }}
		     @include('plan._form')
		  {{ Form::close() }}
  </div>

  </div>
@stop