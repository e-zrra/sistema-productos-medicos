@extends('layouts.master')
@section('content')
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif
<div class="row">

	<div class="col-md-8 col-md-offset-2">
		<h3>
			Nuevo 
		</h3>
		<hr />
		<div>
	  	{{ Form::model($plan, array('route' => 'plan.store')) }}
	    	@include('plan._form')
	  	{{ Form::close() }}
	 	</div>
	</div>
</div>
@stop