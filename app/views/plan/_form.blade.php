<div class="row">
  
  <div class="col-md-6">
    <div class="form-group">
      {{ Form::label ('line_id', 'Linea') }}
      {{ Form::select('line_id', array('0'=> '-- Seleccionar --') + $lines, array('selected' => $plan->line_id),  array('id' => 'line_id', 'class' => 'form-control'))}}
    </div>
    <div class="form-group">
      <div> Fecha de 
        {{ Form::label ('production_date_begin', 'inicio') }} y
        {{ Form::label ('production_date_end', 'fin') }} de la producción
      </div>
      <div class="input-daterange input-group" id="">
        {{ Form::text('production_date_begin', null, array('class' => 'form-control datepicker', 'placeholder' => 'yyyy/mm/dd')) }}
        <span class="input-group-addon">a</span>
        {{ Form::text('production_date_end', null, array('class' => 'form-control datepicker', 'placeholder' => 'yyyy/mm/dd')) }}
      </div>
    </div>
  
    <div class="form-group">
      {{ Form::label ('mold_id', 'Molde') }}
      {{ Form::select('mold_id', array('0'=> '-- Seleccionar --') + $molds, array('selected' => ($plan->model) ? $plan->model->mold->id : ''), array('id' => 'select_mold', 'class' => 'form-control'))}}
    </div>

    <div class="form-group">
      {{ Form::label ('model_id', 'Modelo') }}
      @if(is_null($plan->id))
        {{ Form::select('model_id', array('0'=> '-- Seleccionar --'), null, array('id' => 'select_model', 'class' => 'form-control')) }}
      @else
        {{ Form::select('model_id', array('0'=> '-- Seleccionar --') + $models, array('selected' => $plan->model_id), array('id' => 'select_model', 'class' => 'form-control')) }}
      @endif
    </div>

    <div class="form-group">
      {{ Form::label ('type_id', 'Tipo de producción') }}
      {{ Form::select('type_id', $types, array('selected' => $plan->type_id),  array('id' => '', 'class' => 'form-control'))}}
    </div>
    
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <div> Horario de 
        {{ Form::label ('production_time_begin', 'inicio') }} y
        {{ Form::label ('production_time_end', 'fin') }} de la producción
      </div>
      <div class="input-daterange input-group" id="">
        {{ Form::text('production_time_begin', null, array('id'=>'', 'class' => 'form-control datetimepicker', 'placeholder' => '10:50')) }}
        <span class="input-group-addon">a</span>
        {{ Form::text('production_time_end', null, array('id'=>'', 'class' => 'form-control datetimepicker', 'placeholder' => '10:50')) }}      </div>
    </div>
          
    <div class="form-group">
      {{ Form::label ('quantity', 'Cantidad') }}
      {{ Form::text('quantity', null, array('id'=>'quantity', 'class' => 'form-control')) }}
    </div>

    <div class="form-group">
      {{ Form::label ('comment', 'Comentario') }}
      {{ Form::textarea('comment', null, array('id'=>'comment', 'class' => 'form-control', 'rows' => '5')) }}
    </div>
          
    <div class="form-group">
      <label>Activo: </label>
      {{ Form::label('status_plan', 'Si') }}


      {{ Form::radio('status_plan', '1', ($plan->status_plan == '1' or is_null($plan->id)) ? true : false, array('id'=> 'rb_status_plan')) }}

      {{ Form::label('status_plan', 'No')}}
      {{ Form::radio('status_plan', '2', ($plan->status_plan == '2') ? true : false, array('id'=>'rb_status_plan'))}}
    </div>

    <div class="form-group">
  {{ Form::submit('Guardar', array('class' => 'btn btn-success')) }}
  {{ link_to_route('plan.index', 'Cancelar', null, array('class' => 'text-danger')) }}
</div>
  </div>

</div>



 


