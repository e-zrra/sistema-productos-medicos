@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-md-6">
        <h3>Producción <!-- del día  -->

        <!-- ({{ (Input::get('date')) ? Input::get('date') : date('Y-m-d') }}) -->
        </h3>
    </div>
    
    <div class="col-md-6" id="production-range-datepicker">
        <form action="/production">
            
            <div class="input-daterange input-group input-daterange" >

                <input type="text" value="{{ Input::get('start-date') }}" name="start-date" class="form-control" placeholder="YYYY-MM-DD"  />
                <span class="input-group-addon">a</span>
                <input type="text" value="{{ Input::get('end-date') }}" name="end-date" class="form-control" placeholder="YYYY-MM-DD"  />
                <span class="input-group-btn">
                    <button class="btn btn-default">Buscar</button>
                </span>
            </div>
            <p><a class="advanced_search">Busqueda avanzada</a></p>

            <div class="well container-advanced-search" style="display:{{ (Input::get('active_advanced_search') == 'on') ? 'block' : 'none' }};">
                
                <input type="checkbox" name="active_advanced_search" 
                class="checkbox-advanced-search hidden" checked
                value="{{ (Input::get('active_advanced_search') == 'on') ? 'on' : 'off' }}" />
                
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            Plan de producción
                            {{ Form::select('plan_id', [0 => '-- Selecciona --'] + $plans, Input::get('plan_id'), array('class' => 'form-control')) }}

                        </div>
                       
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            Moldes
                            {{ Form::select('model_id', [0 => '-- Selecciona --'] + $models, Input::get('model_id'), array('class' => 'form-control')) }}

                        </div>
                       
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            Lineas
                            {{ Form::select('line_id', [0 => '-- Selecciona --'] + $lines, Input::get('line_id'), array('class' => 'form-control')) }}

                        </div>
                       
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@if(!Input::get('start-date') || !Input::get('end-date'))
<!-- <p class="alert alert-warning">
    Actualmente este listado está organizado por 
        <b>Línea de producción</b> y <b>Modelo </b>.
</p> -->
@endif

<table class="table production-table">
        
    <thead>
        
        <th>
            Modelo
        </th>
        <th>
            Linea
        </th>
        <th width="150px">
            Plan
        </th>
        <th>
            Total del plan
        </th>
        <th>
            Finalizado
        </th>
        <th>
            Total por cavidad / Inyecciones
        </th>
    </thead>
    <tbody>
        @foreach($productions as $production)

        <tr href="{{ route('production_history', array( ($production->line) ? $production->line->id : $production->line_name, 
                    $production->model_id,
                    date('Y-m-d', strtotime($production->start_date) ),  
                    date('Y-m-d', strtotime($production->end_date) ) ),
                     null) }}">

           
            <td>
                @if($production->plan)
                    @if($production->plan->model)
                        <a href="{{ route('models.show', $production->plan->model->id) }}">
                            {{ $production->plan->model->name }}
                        </a>
                    @else
                        <i class="text-danger">No reconocido el modelo</i>
                    @endif
                @else
                    <i class="text-danger">No reconocido</i>
                @endif
            </td>
            <td>
                <!-- LINEA -->
                @if($production->line)
                    <a href="{{ route('lines.show', $production->line->id) }}">{{ $production->line->name }}</a>
                @else
                    <i class="text-danger">No reconocido ({{ $production->line_id }})</i>
                @endif
            </td>
            <td> <!-- ID DE PLAN --> 
                @if($production->plan)
                    <a href="{{ route('plan.show', $production->plan->id) }}" class="label label-primary" style="font-size:14px;">{{ $production->plan->id }}</a>
                @else
                    <i class="text-danger">No reconocido</i>
                    @if($production->plan_id != 0)
                        <p>
                            <b>Plan antiguo</b> ({{ $production->plan_id }})
                        <p>
                    @endif
                @endif
            </td>
            <td>
                @if($production->plan)
                    <b class="label label-info" style="font-size:16px;">{{ $production->plan->quantity }}</b>
                @else
                    ---
                @endif
            </td>
            <td>
                @if($production->plan)
                    <i class="glyphicon {{ finished_production($production, $production->quantity) }}">
                    </i>
                @else
                    ---
                @endif
            </td>

            <td>
                <div>
                    @if($production->model)
                        <span class="label label-success" style="font-size:16px;">
                        {{ get_total_production($production) }}
                        </span>
                    @else
                        0
                    @endif &nbsp; / &nbsp;
                    <span class="label label-warning" style="font-size:16px;">{{ $production->quantity }}</span>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $productions->links() }}
@if(!$productions->count())
    <p>
        No se encontraron registros
    </p>
@endif
@stop