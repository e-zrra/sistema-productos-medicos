@extends('layouts.master')

@section('content')

    <h3>Historial de la producción</h3>
    <p>
        <a href="{{ URL::previous() }}">Regresar</a>
    </p>
    <table class="table">
        
    <thead>
        <th>
            Linea
        </th>
        <th>
            Plan
        </th>
        <th>
            Fecha
        </th>
        <th>
            Hora
        </th>
        <th>
            Acción
        </th>
    </thead>
    <tbody>
        @foreach($history as $production)
        <tr>
            <td>    
                @if($production->line)
                    <a href="{{ route('lines.show', $production->line->id) }}">
                        {{ $production->line->name }}
                    </a>
                @else
                    <i class="text-danger">No reconocido</i>
                @endif
            </td>
            <td>    
                @if($production->plan)
                    <a href="{{ route('plan.show', $production->plan->id) }}">
                        {{ $production->plan->id }}
                    </a>
                @else
                    <i class="text-danger">No reconocido</i>
                @endif
            </td>
            <td>
                {{ date('d F Y', strtotime($production->created_at)) }}
            </td>

            <td>
                {{ date('h:i:s', strtotime($production->created_at)) }}
            </td>

            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('production.destroy', $production->id))) }}
                    {{ Form::submit('Eliminiar', array('class' => 'btn btn-danger destroy-register'))}}
                {{ Form::close() }}
            </td>

        </tr>
        @endforeach
    </tbody>
    </table>
    <p>
        {{ $history->links() }}
    </p>
@stop