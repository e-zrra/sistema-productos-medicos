$('#production-range-datepicker .input-daterange').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    todayHighlight: true
});

$('.advanced_search').on('click', function () {
	$('.container-advanced-search').toggle();
	if ($('.container-advanced-search').css('display') == 'block') {
		$('.checkbox-advanced-search').prop('checked', true);
		$('.checkbox-advanced-search').val('on');
		//console.log($('.radio-advanced-search').val());
	} else {
		$('.checkbox-advanced-search').prop('checked', false);
		$('.checkbox-advanced-search').val('off');
	}
});

$('.production-table tr').on('click', function () {
	var href = $(this).attr('href');
	window.document.location = href;
})