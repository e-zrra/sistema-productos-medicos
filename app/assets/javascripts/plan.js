$('#select_mold').change(function () {
	var mold_id = $('#select_mold option:selected').val();

	if (mold_id === 0) {
		$('#select_model').html('<option value="0" selected="selected">-- Seleccionar --</option>');
	};
	$.ajax({
		url: "/plan_mold/" + mold_id,
		beforeSend: function () {
		},error: function () {
		}, success: function (res) {
			console.log(res);
			if (res !== null) {

				var option = '';

				$('#select_model').html('<option value="0" selected="selected">-- Seleccionar --</option>');

				$.each(res, function (e, i) {
					
					//console.log(i);

					option += '<option value="' + i.id + '" selected="selected">' + i.name + '</option>';
				});

				$('#select_model').append(option);

				$('#select_model option:first-child').attr('selected', 'selected');

			} else {

				$('#select_model').html('<option value="0" selected="selected">-- Seleccionar --</option>');

				alert('No hay modelos asignados');
			} 
		}
	});


});


$('.datepicker').datepicker({
    format: 'yyyy/mm/dd',
    startDate: '0d',
    autoclose: true,
    todayHighlight: true
});



$('.datetimepicker').datetimepicker({
    pickDate: false,
        pickSeconds: false,
        pick12HourFormat: false,
    format: 'HH:mm',
});


$('#sandbox-container .input-daterange').datepicker({
	format: 'yyyy/mm/dd',
    startDate: '0d',
    autoclose: true,
    todayHighlight: true
});