<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('plan', function ($table) {
			$table->create();
			$table->increments('id');
			$table->integer('model_id');
			$table->integer('user_id')->nullable();
			$table->integer('line_id')->nullable();
			$table->integer('shift_id')->nullable();
			$table->date('production_date_begin')->nullable();
			$table->date('production_date_end')->nullable();
			$table->time('production_time_begin')->nullable();
			$table->time('production_time_end')->nullable();
			$table->text('comment')->nullable();
			$table->text('order')->nullable();
			$table->integer('quantity')->nullable();
			$table->integer('status_plan');
			$table->integer('type_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('plan');
	}

}
