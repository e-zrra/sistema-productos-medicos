<?php

use Illuminate\Database\Migrations\Migration;

class CreateModelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('models', function  ($table) {
			$table->create();
			$table->increments('id');
			$table->string('name');
			$table->text('description')->nullable();
			$table->integer('mold_id');
			$table->integer('cavity')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
		//
		Schema::drop('models');
	}

}