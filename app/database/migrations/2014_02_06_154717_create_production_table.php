<?php

use Illuminate\Database\Migrations\Migration;

class CreateProductionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('productions', function ($table) {
			$table->create();
			$table->increments('id');
			$table->integer('user_id')->nullable();
			$table->integer('line_id')->nullable();
			$table->integer('plan_id')->nullable();
			$table->integer('model_id')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('productions');
	}

}