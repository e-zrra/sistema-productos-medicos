<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefectiveProductionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('defective_production', function ($table) {
			$table->create();
			$table->increments('id');
			$table->integer('line_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('model_id');
			$table->integer('defect_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('defective_production');
	}

}
