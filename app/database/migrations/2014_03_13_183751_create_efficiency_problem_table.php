<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEfficiencyProblemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('efficiency_defects', function ($table) {
			$table->create();
			$table->increments('id');
			$table->integer('efficiency_id')->nullable();
			$table->integer('defect_id')->nullable();
			$table->integer('quantity')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('efficiency_defects');
	}

}
