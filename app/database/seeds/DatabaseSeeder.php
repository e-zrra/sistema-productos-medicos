<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		/*$this->call('UserTypesTableSeeder');

		$this->call('UsersTableSeeder');*/

		$this->call('PlanTypesTableSeeder');

	}

}