<?php 

class PlanTypesTableSeeder extends Seeder {

	public function run()
	{

		PlanType::truncate();
		
		PlanType::create(array(
			'name' 			=> 	'Cavidad'
		));

		PlanType::create(array(
			'name' 			=> 	'Injección'
		));

	}

}


