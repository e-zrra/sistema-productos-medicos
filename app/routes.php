<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('login',  array('as' => 'login.index', 'uses' => 'AuthController@index'));

Route::post('login', array('as' => 'login.signin', 'uses' => 'AuthController@sign_in'));

//Route::group(array('before' => 'auth'), function () {

	Route::get('/', array('as' => 'home.index', 'uses' => 'HomeController@index'));

	Route::get('perfil', array('as' => 'perfil', 'uses' => 'UsersController@perfil'));

	Route::get('logout', array('as' => 'login.logout', 'uses' => 'AuthController@sign_out'));

	Route::resource('users', 'UsersController');

	Route::resource('molds', 'MoldsController');

	Route::resource('lines', 'LinesController');

	Route::resource('efficiency', 'EfficiencyController');

	Route::resource('devices', 'DevicesController');

	Route::resource('production', 'ProductionController');

	Route::get('production/history/{line_id}/{model_id?}/{start_date?}/{end_date?}',
		array(
			'as' => 'production_history',
			'uses' => 'ProductionController@history')
		);

	Route::resource('defects', 'DefectsController');

	Route::resource('models', 'ModelsController');

	Route::resource('plan', 'PlanController');

	Route::get('plan_mold/{mold_id}', 'PlanController@plan_mold');

//});

/*Route::filter('auth', function()
{
    if (Auth::guest()) return Redirect::to('login');
});*/

Route::get('register_production', 'ProductionController@register_production');

$path =  Request::segment(1);

Session::put('nav-option', $path);

//no mismo plan en el mismo horario dependiendo de la linea

//localhost:8000/register_production?line_id=1&arduino_id=1

