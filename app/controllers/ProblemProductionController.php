<?php
class ProblemProductionController extends BaseController {

   private $autorizado;

   public function __construct(){
      $this->autorizado = (Auth::user()->usertype_id == 1);
   }
   public function index() {
      $this->autorizado = (Auth::user()->usertype_id == 1);

      $pp = ProblemProduction::all();//orderBy('number', 'ASC')->get();
      return View::make('problemProduction.index')->with('pp', $pp)->with('autoriz', $this->autorizado);
   }

   public function create() {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $pp = new ProblemProduction();
      return View::make('problemProduction.save')->with('pp',$pp);
   }

   public function store() {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $pp = new ProblemProduction();
      $pp->name = Input::get('name');
      $pp->color_identify = Input::get('color');
      $pp->pp_description = Input::get('pp_description');

      
      $validator = ProblemProduction::validate(array(
         'name' => Input::get('name'),
         'pp_description' => Input::get('pp_description'),
      ));

      if ($validator->fails()) {
         $errors = $validator->messages()->all();
         return View::make('problemproduction.save')->with('pp', $pp)->with('errors', $errors);
      }else{
         $pp->save();
         return Redirect::to('problemproduction')->with('notice', 'El PP ha sido creado correctamente');///////////////
      }
   }

   public function edit($id) { 
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $pp = ProblemProduction::find($id);
      return View::make('problemproduction.save')->with('pp', $pp);
   }

   public function update($id) { 
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $pp = ProblemProduction::find($id);
      $pp->name = Input::get('name');
      $pp->color_identify = Input::get('color');
      $pp->pp_description = Input::get('pp_description');
      
      /*$validator = Defect::validate(array(
         'name' => Input::get('name'),
         'def_description' => Input::get('def_description'),
      ), $defect->id);*/
   
      /*if($validator->fails()){
         $errors = $validator->messages()->all();
         return View::make('defects.save')->with('defect', $defect)->with('errors', $errors);
      }else{*/
         $pp->save();
         return Redirect::to('problemproduction')->with('notice', 'El PP ha sido modificado correctamente');////////////////
      //}
   }
   /*public function destroy($id) {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $defect = Defect::find($id);
      $defect->delete();
      return Redirect::to('defects')->with('notice', 'El defecto se elimino');  
   }*/
}
?>