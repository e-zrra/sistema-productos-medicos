<?php

class HomeController extends BaseController {

	public function index()
	{

        $lines = Line::all();

        $productions = Production::select('*', 
                DB::raw('count(line_id) as quantity, line_id as line_name, MIN(created_at) as start_date, MAX(created_at) as end_date ')) //, MAX(created_at) as last_row
                ->groupBy('line_id', 'model_id', 'plan_id')
                ->where(DB::raw('date(created_at)'), date('Y-m-d'))
                ->take(3)
                ->where('plan_id', '>', 0)
                ->get();
            
            return View::make('home.index', compact('productions', 'lines'));
		
	}

    public function register_production() {

        /*try {

            $production = new Production;

            $line_id = Input::get('line_id');


            $plan = Plan::where('line_id', $line_id)
                    ->where('production_date_begin', '<=', date('Y-m-d'))
                    ->where('production_date_end',   '>=', date('Y-m-d'))
                    ->where('production_time_begin', '<=', date('H:i:s'))
                    ->where('production_time_end',   '>=', date('H:i:s'))
                    ->first();

            if (isset($plan->line_id)) {

                $production->plan_id = $plan->id;

            } else {

                $production->plan_id = 0;
            }

            $production->line_id = $line_id;

            $production->arduino_id = Input::get('arduino_id');

            $production->save();
            
        } catch (Exception $e) {
            
            return 'Error al registrar!'.$e;
        }

        return 'Registro agregado!';*/
    }

}
