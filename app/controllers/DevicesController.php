<?php
class DevicesController extends BaseController {

   private $autorizado;

   public function __construct(){
      
   }

   public function index () {

      if (Input::get('search')) {
         
         $search = Input::get('search');

         $devices = Device::where('name', 'LIKE', '%'. $search. '%')->orWhere('description', 'LIKE', '%' .$search. '%')->paginate(5);

         $devices->appends(Input::only('search'));

      } else {

         $devices = Device::paginate(12);
      }


      return View::make('devices.index', compact('devices'));
   }

   public function show($id) {

   }
   public function create () {

      $device = new Device();

      return View::make('devices.save', compact('device'));
   }

   public function store() {

      try {

         $Device = new Device();

         $Device->id = Input::get('id');

         $Device->name = Input::get('name');

         $Device->description = Input::get('description');
         
         $Device->save();
         
      } catch (Exception $e) {
         
         return Redirect::back();
      }
      
      return Redirect::to('devices')->with('notice', 'El dispositivo ha sido creada correctamente');

   }

   public function edit($id) {

      try {

         $device = Device::find($id);
         
      } catch (Exception $e) {
         
         return Redirect::back();
      }

      return View::make('devices.edit', compact('device'));
      
   }

   public function update($id) {

      try {

         $device = Device::find($id);

         $device->id = Input::get('id');

         $device->name = Input::get('name');

         $device->description = Input::get('description');
      
         $device->save();

      } catch (Exception $e) {
      
         return Redirect::back()->withInput();   
      }
      
      return Redirect::to('devices')->with('notice', 'La linea ha sido modificada correctamente');

   }

   public function destroy ($id) {
      
     try {

         $device = Device::find($id);
         $device->delete();
         
      } catch (Exception $e) {
         
         return Redirect::back()->with('notice', 'No se pudo eliminar el registro');  
      }
      
      return Redirect::to('devices')->with('notice', 'La linea se elimino');  
   }


}
?>