<?php
class MoldsController extends BaseController {

   private $autorizado;

   public function __construct(){
   }
   
   public function index() {

      if (Input::get('search')) {
         
         $search = Input::get('search');

         $molds = Mold::where('name', 'LIKE', '%'. $search. '%')->orWhere('description', 'LIKE', '%' .$search. '%')->paginate(5);

         $molds->appends(Input::only('search'));

      } else {

         $molds = Mold::paginate(12);
      }
      return View::make('molds.index', compact('molds'));
   }

   public function show($id) {
   }
   public function create() {
      
      $mold = new Mold();

      return View::make('molds.save', compact('mold'));
   }

   public function store() {

      try {
      
         $inputs = Input::only('name', 'description');

         $mold = new Mold();
         
         $mold->name = $inputs['name'];

         $mold->description = $inputs['description'];

         $mold->save();
         
      } catch (Exception $e) {
         
         return Redirect::back();
      }
      
      return Redirect::to('molds')->with('notice', 'El molde ha sido creado correctamente');;
   }
   public function edit($id) {

      $mold = Mold::find($id);
      
      return View::make('molds.edit')->with('mold', $mold);
   }

   public function update($id) { 

      try {

         $mold = Mold::find($id);

         $mold->name = Input::get('name');
         
         $mold->description = Input::get('description');

         $mold->save();
         
      } catch (Exception $e) {
         
         return Redirect::back();
      }
      
      
      return Redirect::to('molds')->with('notice', 'El molde ha sido actualizado correctamente');
   }
   public function destroy($id) {

      try {

         $mold = Mold::find($id);
      
         $mold->delete();
         
      } catch (Exception $e) {
         
         return Redirect::back();
      }
      
      return Redirect::to('molds')->with('notice', 'El molde se elimino');  
   }
}
?>