<?php
class LinesController extends BaseController {

   public function __construct(){

   }
   public function index() {
      
      $lines = Line::paginate(12);

      return View::make('lines.index', compact('lines'));
   }

   public function show($id) {
      
      $line = Line::find($id);

      return View::make('lines.show', array('line' => $line ));
   }
   public function create() {

      $devices = Device::lists('name', 'id');

      $line = new Line();
      
      return View::make('lines.save', compact('line', 'devices'));
   }

   public function store() {

      try {

         $line = new Line();

         $line->id = Input::get('id');

         $line->name = Input::get('name');

         $line->description = Input::get('description');

         $line->device_id = Input::get('device_id');
         
         $line->save();
         
      } catch (Exception $e) {

         return 'Error: ' . $e;
         
         return Redirect::back();
      }
      
      return Redirect::to('lines')->with('notice', 'La linea ha sido creada correctamente');
      
   }
   public function edit($id) {

      $devices = Device::lists('name', 'id');

      try {

         $line = Line::find($id);

      } catch (Exception $e) {
         
         return Redirect::back();
      }

      return View::make('lines.edit', compact('line', 'devices'));
   }

   public function update($id) {

      try {

         $line = Line::find($id);

         $line->id = Input::get('id');

         $line->name = Input::get('name');

         $line->description = Input::get('description');

         $line->device_id = Input::get('device_id');
      
         $line->save();

      } catch (Exception $e) {
      
         return Redirect::back();   
      }
      
      return Redirect::to('lines')->with('notice', 'La linea ha sido modificada correctamente');
   }
   
   public function destroy($id) {
      
      try {

         $line = Line::find($id);
         
         $line->delete();
         
      } catch (Exception $e) {
         
         return Redirect::back()->with('notice', 'No se pudo eliminar el registro');  
      }
      
      return Redirect::to('lines')->with('notice', 'La linea se elimino');  
   }
}
?>