<?php
class DefectsController extends BaseController {

   private $autorizado;

   public function __construct(){
      $this->autorizado = (Auth::user()->usertype_id == 1);
   }
   public function index() {
      $this->autorizado = (Auth::user()->usertype_id == 1);
      $defects = Defect::orderBy('number', 'ASC')->with('problemproduction')->get();
      return View::make('defects.index')->with('defects', $defects)->with('autoriz', $this->autorizado);
   }

   public function create() {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $defect = new Defect();
      $pp = ProblemProduction::lists('name', 'id');
      return View::make('defects.save')->with('defect',$defect)->with('pp', $pp);
   }

   public function store() {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $defect = new Defect();
      $defect->number = Input::get('number');
      $defect->name = Input::get('name');
      $defect->problemproduction_id = Input::get('problemproduction_id');
      $defect->def_description = Input::get('def_description');

      
      $validator = Defect::validate(array(
         'name' => Input::get('name'),
         'def_description' => Input::get('def_description'),
      ));

      if ($validator->fails()) {
         $errors = $validator->messages()->all();
         return View::make('defects.save')->with('defect', $defect)->with('errors', $errors);
      }else{
         $defect->save();
         return Redirect::to('defects')->with('notice', 'El defecto ha sido creado correctamente');///////////////
      }
   }

   public function edit($id) { 
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $defect = Defect::find($id);
      $pp = ProblemProduction::lists('name', 'id');
      return View::make('defects.save')->with('defect', $defect)->with('pp', $pp);
   }

   public function update($id) { 
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $defect = Defect::find($id);
      $defect->number = Input::get('number');
      $defect->name = Input::get('name');
      $defect->problemproduction_id = Input::get('problemproduction_id');
      $defect->def_description = Input::get('def_description');
      
      $validator = Defect::validate(array(
         'name' => Input::get('name'),
         'def_description' => Input::get('def_description'),
      ), $defect->id);
   
      if($validator->fails()){
         $errors = $validator->messages()->all();
         return View::make('defects.save')->with('defect', $defect)->with('errors', $errors);
      }else{
         $defect->save();
         return Redirect::to('defects')->with('notice', 'El defecto ha sido modificado correctamente');////////////////
      }
   }
   public function destroy($id) {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $defect = Defect::find($id);
      $defect->delete();
      return Redirect::to('defects')->with('notice', 'El defecto se elimino');  
   }
}
?>