<?php
class ShiftsController extends BaseController {

   private $autorizado;

   public function __construct(){
      $this->autorizado = (Auth::user()->usertype_id == 1);
   }
   public function index() {
      $shifts = Shift::all();
      return View::make('shifts.index')->with('shifts', $shifts)->with('autoriz', $this->autorizado);
   }

   public function show($id) {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $shift = Shift::find($id);
      return View::make('shifts.show')->with('shift', $shift);
   }
   public function create() {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $shift = new Shift();
      return View::make('shifts.save')->with('shift',$shift);
   }

   public function store() {

      
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $Shift = new Shift();
      $Shift->shi_description = Input::get('shi_description');
      $shift->color_identify = Input::get('color');

      $validator = Shift::validate(array(
         'shi_description' => Input::get('shi_description')
      ));

      if ($validator->fails()) {
         $errors = $validator->messages()->all();
         return View::make('shifts.save')->with('shift', $Shift)->with('errors', $errors);
      }else{
         $Shift->save();
         return Redirect::to('shifts')->with('notice', 'El Turno ha sido creado correctamente');////////////////////////////
      }
   }
   public function edit($id) { 
      
      $shift = Shift::find($id);
      return View::make('shifts.save')->with('shift', $shift);
   }

   public function update($id) { 
      
      
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $shift = Shift::find($id);
      $shift->shi_description = Input::get('shi_description');
      $shift->color_identify = Input::get('color');
      $shift->start_time = Input::get('start_time');
      $shift->finish_time = Input::get('finish_time');
      
      $validator = shift::validate(array(
         'shi_description' => Input::get('shi_description'),
      ), $shift->id);
      
      if($validator->fails()){
         $errors = $validator->messages()->all();
         return View::make('shifts.save')->with('shift', $shift)->with('errors', $errors);
      }else{
         $shift->save();
         return Redirect::to('shifts')->with('notice', 'El turno ha sido modificado correctamente');/////////////////////
      }
   }

   public function destroy($id) {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $shift = Shift::find($id);
      $shift->delete();
      return Redirect::to('shifts')->with('notice', 'El turno se elimino');  ////////////////////////////
   }
}
?>