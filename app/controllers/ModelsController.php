<?php
class ModelsController extends BaseController {

   private $autorizado;

   public function __construct() {

   }
   
   public function index() {

      if (Input::get('search')) {

         $search = Input::get('search');

         $models = Model::where('name', 'LIKE', '%'. $search .'%')->orWhere('description', 'LIKE', '%'. $search. '%')->paginate(12);

         $models->appends(Input::only('search'));

      } else {

         $models = Model::paginate(12);

      }

      return View::make('models.index', compact('models'));
   }

   public function show($id) {

      $model = Model::find($id);

      return View::make('models.show', compact('model'));
   }

   public function create() {

      $model = new Model();
      $molds = Mold::lists('name', 'id');
      return View::make('models.save', compact('molds', 'model'));
   }

   public function store() {

      try {

         $model = new Model();

         $model->name = Input::get('name');

         $model->description = Input::get('description');

         $model->cavity = Input::get('cavity');

         $model->mold_id = Input::get('mold_id');

         $model->save();
         
      } catch (Exception $e) {
         
         return Redirect::back();
      }

      return Redirect::to('models')->with('notice', 'El modelo ha sido creado correctamente');
   }
   public function edit($id) {

      $model = Model::find($id);
      
      $molds = Mold::lists('name', 'id');
      
      return View::make('models.edit', compact('model', 'molds'));
   }

   public function update($id) {

      try {

         $model = Model::find($id);

         $model->name = Input::get('name');
         $model->description = Input::get('description');
         $model->mold_id = Input::get('mold_id');
         $model->cavity = Input::get('cavity');
         $model->save();
         
      } catch (Exception $e) {
         
         return Redirect::back();
      }
      return Redirect::to('models')->with('notice', 'El modelo ha sido modificado correctamente');
   }
   public function destroy($id) {

      try {
         
         $model = Model::find($id);

         $model->delete();
         
      } catch (Exception $e) {
         
         return Redirect::back();
      }

      return Redirect::back()->with('notice', 'Registro eliminado');
   }
}
?>