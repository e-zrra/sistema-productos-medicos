<?php
class PlanController extends BaseController {

   private $autorizado;

   public function __construct(){

      //$path =  Request::segment(1);
   }

   public function index() {
      
      $plans = Plan::orderBy('line_id')->paginate(12);

      return View::make('plan.index', compact('plans'));
   }

   public function create () {

      $types = PlanType::lists('name', 'id');

      $plan = new Plan();

      $lines = Line::lists('name', 'id');

      $molds = Mold::lists('name', 'id');

      return View::make('plan.save', compact('plan', 'lines', 'molds', 'types'));
   }

   public function show ($id) {

      $plan = Plan::find($id);

      return View::make('plan.show', compact('plan'));

   }

   public function plan_mold ($mold_id) {

      $models = Model::where('mold_id', $mold_id)->get();

      return $models;
   }

   public function store () {

      $inputs = Input::all();

      //validar el horario si ya existe un plan con la misma hora o cruza con el horario de otro plan

      //VALIDAR si la hora iniciar es menor que la que finaliza y la fecha iniciar es menor que la que finaliza

      /*if ($inputs['production_date_end'] > $inputs['production_date_begin']) {

         return 'aqui';
         
         return Redirect::back()->withInput();
      }

      if ($inputs['production_time_end'] > $inputs['production_time_begin']) {
            return;
         return Redirect::back()->withInput();
      }*/

      try {

         $plan = new Plan();

         $plan->model_id = $inputs['model_id'];

         $plan->type_id = $inputs['type_id'];

         $plan->line_id = $inputs['line_id'];

         $plan->production_date_begin = $inputs['production_date_begin'];

         $plan->production_date_end = $inputs['production_date_end'];

         $plan->comment = $inputs['comment'];

         $plan->quantity = $inputs['quantity'];

         $plan->status_plan = $inputs['status_plan'];

         $plan->production_time_begin = $inputs['production_time_begin'];

         $plan->production_time_end = $inputs['production_time_end'];

         $plan->save();
         
      } catch (Exception $e) {

         return $e;
         return Redirect::back();
      }

      return Redirect::to('plan')->with('notice', 'El plan ha sido creado correctamente');

   }

   public function edit ($id) {

      $types = PlanType::lists('name', 'id');

      try {

         $plan = Plan::find($id);

         $lines = Line::lists('name', 'id');

         $models = Model::where('id', $plan->model_id)->lists('name', 'id');

         $molds = Mold::lists('name', 'id');
         
      } catch (Exception $e) {

         return Redirect::back();
      }

      return View::make('plan.edit', compact('plan', 'models', 'molds', 'lines', 'types'));
   }

   public function update ($id) {
      
      $inputs = Input::all();

      try {

         $plan = Plan::find($id);

         $plan->model_id = $inputs['model_id'];

         $plan->type_id = $inputs['type_id'];

         $plan->line_id = $inputs['line_id'];

         $plan->production_date_begin = $inputs['production_date_begin'];

         $plan->production_date_end = $inputs['production_date_end'];

         $plan->comment = $inputs['comment'];

         $plan->quantity = $inputs['quantity'];

         if (isset($inputs['status_plan'])) {
            
            $plan->status_plan = $inputs['status_plan'];
         }

         $plan->production_time_begin = $inputs['production_time_begin'];

         $plan->production_time_end = $inputs['production_time_end'];

         $plan->save();
         
      } catch (Exception $e) {

         return Redirect::back();
      }

      return Redirect::to('plan')->with('notice', 'El plan ha sido actualizado correctamente');

   }

   public function destroy ($id) {

      try {

            $plan = Plan::find($id);

            $plan->delete();

      } catch (Exception $e) {
         
         return Redirect::back();
      }
      return Redirect::back()->with('notice', 'Registro eliminado');
   }
}
?>