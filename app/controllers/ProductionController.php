<?php

class ProductionController extends BaseController {
		
	public function index() {

        $plans = Plan::lists('id', 'id');

        $models = Model::lists('name', 'id');

        $lines = Line::lists('name', 'id');

        $start_date = '';

        $end_date = '';

        if (Input::get('start-date') or Input::get('end-date') ) {

            $start_date = Input::get('start-date');

            $end_date = Input::get('end-date');

        }

        $productions = Production::select('*', 
                DB::raw('count(line_id) as quantity, line_id as line_name, MIN(created_at) as start_date, MAX(created_at) as end_date ')) //, MAX(created_at) as last_row
                ->groupBy('line_id', 'model_id', 'plan_id')
                ->where(function ($query) use ($start_date, $end_date) {

                    if ($start_date and $end_date) {
                        $query->whereBetween(DB::raw('date(created_at)'), array($start_date, $end_date));
                    }

                    if (Input::get('active_advanced_search') == 'on') {

                        if (Input::get('line_id') ) {
                            $query->where('line_id', Input::get('line_id'));
                        }

                        if (Input::get('model_id') ) {
                            $query->where('model_id', Input::get('model_id'));
                        }

                        if (Input::get('plan_id') ) {
                            $query->where('plan_id', Input::get('plan_id'));
                        }
                    }

                    

                })->paginate(12);
            
            return View::make('production.index', compact('productions', 'plans', 'models', 'lines'));
		
	}

    public function history ($line_id, $model_id = null,  $start_date = null, $end_date = null) {

        $history = Production::where('line_id', $line_id)
                ->where('model_id', $model_id)
                ->where(function ($query) use ($start_date, $end_date) {

                    $query->whereBetween(DB::raw('date(created_at)'), array($start_date, $end_date));
                    
                })->paginate(12);
            
            return View::make('production.history', compact('history'));

    }

	public function register_production() {

        try {
            
            $production = new Production;

            $line_id = Input::get('line_id');

            return $plan = Plan::where('line_id', $line_id)
                    ->where('production_date_begin', '<=', date('Y-m-d'))
                    ->where('production_date_end',   '>=', date('Y-m-d'))
                    ->where('production_time_begin', '<=', date('H:i:s'))
                    ->where('production_time_end',   '>=', date('H:i:s'))
                    ->where('status_plan', 1)
                    ->first();

            if (!is_null($plan)) {

                $production->plan_id = $plan->id;

                $production->model_id = $plan->model->id;

                $production->line_id = $line_id;

            } else {

                $production->plan_id = 0;

                $production->model_id = 0;

                $production->line_id = $line_id;
            }

            $production->save();
            
        } catch (Exception $e) {

            return 'Error al registrar!'.$e;
        }

        return 'Registro agregado!';
    }
    public function destroy ($id) {

      try {

            $production = Production::find($id);

            $production->delete();

      } catch (Exception $e) {
         
         return Redirect::back();
      }
      return Redirect::back()->with('notice', 'Registro eliminado');
   }


}