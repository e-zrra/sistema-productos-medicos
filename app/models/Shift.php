<?php
	class Shift extends Eloquent {
		/*public function user(){
			return $this->belongsTo('User');
		}*/
		protected $table = 'shifts';

		public static $rules = array(
	      'shi_description' => 'required|min:2',
	   );

		public static $messages = array( /////////////////////////Edit/////////////////////////////////
	      	'shi_description.required' => 'Requerido'
	   	);
	   	
		public static function validate($data, $id=null){
	      $reglas = self::$rules;
	      $messages = self::$messages;
	      return Validator::make($data, $reglas, $messages);
	   }
		
	}
?>