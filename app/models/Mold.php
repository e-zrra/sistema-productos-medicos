<?php
class Mold extends Eloquent{

	public function models() {
		return $this->hasMany('Model');
	}

	protected $table = 'molds';

	public static $rules = array(
      'codigo' => 'required|min:2',
      'mol_description' => 'required'
   );
	
}
?>