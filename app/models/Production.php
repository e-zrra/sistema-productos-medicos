<?php
	class Production extends Eloquent {

		public function line() {
			return $this->belongsTo('Line', 'line_id', 'id');
		}

		public function user() {
			return $this->belongsTo('User');
		}

		public function plan () {
			return $this->belongsTo('Plan', 'plan_id', 'id');
		}

		public function model () {
			return $this->belongsTo('Model', 'model_id', 'id');
		}

		protected $table = 'productions';

		public function get_total () {

			
			
		}
	}
?>