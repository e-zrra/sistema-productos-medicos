<?php
	class Defect extends Eloquent {
		/*public function production(){
			return $this->hasMany('Production', 'arduino_id');
		}*/

		protected $table = 'defects';

		public static $rules = array(
	      'name' => 'required|min:2',
	      'def_description' => 'required|min:2',
	   );

		public static $messages = array( /////////////////////////Edit/////////////////////////////////
	      	'name.required' => 'Requerido',
	      	'def_description.required' => 'Requerido'
	   	);

	   	public function problemproduction (){
			return $this->belongsTo('problemproduction');
		}

		public static function validate($data, $id=null){
	      $reglas = self::$rules;
	      $messages = self::$messages;
	      return Validator::make($data, $reglas, $messages);
	   }
	}
?>