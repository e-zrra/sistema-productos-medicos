<?php
	class Plan extends Eloquent {
		/*public function production(){
			return $this->belongsTo('Production');
		}*/
		public function line() {
			return $this->belongsTo('Line');
		}
		public function user() {
			return $this->belongsTo('User');
		}
		public function model() {
			return $this->belongsTo('Model');
		}
		
		public function shift() {
			return $this->belongsTo('Shift');
		}

		public function type () {
			return $this->belongsTo('PlanType', 'type_id', 'id');
		}

		protected $table = 'plan';
	}
?>