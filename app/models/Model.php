<?php
	class Model extends Eloquent {

		public function mold() {
			return $this->belongsTo('Mold', 'mold_id', 'id');
		}

		protected $table = 'models';

	}
?>