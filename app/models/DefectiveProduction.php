<?php
	class DefectiveProduction extends Eloquent {

		public function line() {
			return $this->belongsTo('Line');
		}
		public function user() {
			return $this->belongsTo('User');
		}
		public function model() {
			return $this->belongsTo('Model');
		}
		public function defect() {
			return $this->belongsTo('Defect');
		}

		protected $table = 'defective_production';
	}
?>