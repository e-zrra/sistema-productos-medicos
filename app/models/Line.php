<?php
	class Line extends Eloquent {
		
		/*public function production(){
			return $this->belongsTo('Production');
		}*/
		public function modelos() {
			//return $this->belongsToMany('Model', 'productions', 'line_id', 'model_id');
		}

		public function plans() {
			return $this->hasMany('Plan');
		}

		public function device () {
			return $this->belongsTo('Device', 'device_id', 'id');
		}
		protected $table = 'lines';

	}
?>