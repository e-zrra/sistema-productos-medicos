<?php
	class EfficiencyDefects extends Eloquent {

		public function defect() {
			return $this->belongsTo('Defect');
		}

		protected $table = 'efficiency_defects';
	}
?>