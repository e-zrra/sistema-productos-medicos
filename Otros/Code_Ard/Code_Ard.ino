#include <Keypad.h>
#include <SPI.h>
#include <Ethernet.h> //Lib Ethernet
byte mac[] = {0x90, 0xA2, 0xDA, 0x0D, 0xC2, 0x94}; //MAC SHIELD

//KEYPAD
const byte ROWS = 4;
const byte COLS = 4;

String user_id; //ID USER
String line_id; //ID LINE
String id_arduino = "1"; //ID ARDUINO
String id_linea = "16";
String id_user = "1";//Cambiar a codigo de empleado
boolean active = false;

#define sensor 5
#define led_green 3
#define led_red 13

//KEYPAD MATRIS
char keys[ROWS][COLS] = {
  {'1','4','7','S'},
  {'2','5','8','0'},
  {'3','6','9','R'},
  {'A','B','C','D'}
};

//KEYPAD PINS USING
byte rowPins[ROWS] = {9, 8, 7, 6}; 
byte colPins[COLS] = {13, 12, 11, 10};

//KEYPAD MAP
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

//
byte server[] = {192, 168, 1, 73}; //IP SEVIDOR
IPAddress ip(192,168,1,10); //IP ARDUINO
String ipServer = "192.168.1.73"; //IP ADDRESS SERVER STRING 

EthernetClient client; //CLINETE ETHERNET

void setup() {
    Serial.begin(9600);
    pinMode(sensor, OUTPUT);
    pinMode(led_red, OUTPUT);
    pinMode(led_green, OUTPUT);
}

void loop()
{
  if(digitalRead(sensor) == HIGH){
    delay(500);
    Serial.print("Activo");
    delay(500);
    connection(id_arduino, id_linea, id_user);
  }else{
  }
}

void client_available(){
  delay(1500);
  if (!client.connected()) {
    Serial.println("Desconnect client!");
  } else if (client.available()) {
    char c = client.read();
    Serial.println(c);
    digitalWrite(led_red, HIGH);
    delay(500);
    digitalWrite(led_green, LOW);
  }else{
    Serial.println("Unavailable");
    digitalWrite(led_green, LOW);
    //digitalWrite(sensor, LOW);
    //digitalWrite(led_green, LOW);
    //digitalWrite(led_red, HIGH);
  }
}

void connection(String numero, String linea, String user_id){
  //delay(2000);
  Ethernet.begin(mac, ip);
  Serial.println("connecting...");
  if(client.connect(server, 80)) { //Edit or add port of server
    Serial.println("connected");
    client.println("GET http://"+ipServer+"/dcm/arduino/insertArduino.php?userCode="+user_id+"&arduinoId="+numero+"&lineId="+linea);
    Serial.println("GET http://"+ipServer+"/dcm/arduino/insertArduino.php?userCode="+user_id+"&arduinoId="+numero+"&lineId="+linea);
    client.println("Connection: close");
    client.println();
    client_available();
    client.stop();
  } else {
    Serial.println("connection failed");
  }
}

