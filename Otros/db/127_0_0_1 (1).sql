-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-03-2014 a las 01:00:56
-- Versión del servidor: 5.5.32
-- Versión de PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `dcm`
--
CREATE DATABASE IF NOT EXISTS `dcm` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `dcm`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `arduinos`
--

CREATE TABLE IF NOT EXISTS `arduinos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ard_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `arduinos`
--

INSERT INTO `arduinos` (`id`, `ard_description`, `created_at`, `updated_at`) VALUES
(1, 'Arduino UNO\r\n', '2014-02-14 17:19:19', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `defective_production`
--

CREATE TABLE IF NOT EXISTS `defective_production` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `line_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `defect_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `defective_production`
--

INSERT INTO `defective_production` (`id`, `line_id`, `user_id`, `model_id`, `defect_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, '2014-02-20 16:22:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `defects`
--

CREATE TABLE IF NOT EXISTS `defects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `def_description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `defects`
--

INSERT INTO `defects` (`id`, `name`, `def_description`, `created_at`, `updated_at`) VALUES
(1, 'Golpe', 'Se golpeo muy fuerte!', '2014-02-20 08:00:00', '0000-00-00 00:00:00'),
(2, 'Rayado', 'Se rayo bien machin!', '2014-02-20 10:04:08', '0000-00-00 00:00:00'),
(3, 'dcd', 'cd', '2014-03-11 03:48:10', '2014-03-11 03:48:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lines`
--

CREATE TABLE IF NOT EXISTS `lines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lin_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `lines`
--

INSERT INTO `lines` (`id`, `lin_description`, `created_at`, `updated_at`) VALUES
(1, 'Ensamble de b-cover', '2014-02-15 06:48:27', '2014-02-15 06:48:27'),
(2, 'Prensado', '2014-02-22 02:07:57', '2014-03-04 00:23:09'),
(3, 'Otra linea', '2014-03-02 01:51:35', '2014-03-02 01:51:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_01_28_181242_create_users_table', 1),
('2014_01_29_224745_create_post_table', 1),
('2014_02_05_163618_create_models_table', 1),
('2014_02_05_215650_create_molds_table', 1),
('2014_02_06_152753_create_lines_table', 1),
('2014_02_06_154717_create_production_table', 1),
('2014_02_06_163027_create_arduinos_table', 1),
('2014_02_14_202955_create_plan_table', 1),
('2014_02_15_195318_create_shifts_table', 2),
('2014_02_20_212015_create_defects_table', 3),
('2014_02_20_212220_create_defective_production_table', 3),
('2014_03_07_223506_create_type_user_table', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `models`
--

CREATE TABLE IF NOT EXISTS `models` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mod_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mold_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `models`
--

INSERT INTO `models` (`id`, `codigo`, `mod_description`, `mold_id`, `created_at`, `updated_at`) VALUES
(8, 'Modelo', 'Modelo', 2, '2014-03-11 01:10:32', '2014-03-12 07:04:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `molds`
--

CREATE TABLE IF NOT EXISTS `molds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mol_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `molds`
--

INSERT INTO `molds` (`id`, `codigo`, `mol_description`, `created_at`, `updated_at`) VALUES
(1, 'Molde1', 'Molde numero 1', '2014-02-15 06:46:03', '2014-02-15 06:46:03'),
(2, 'Molde 2', 'Molde 2', '2014-02-15 06:46:16', '2014-03-01 06:18:10'),
(3, 'dd', 'd', '2014-03-11 02:40:31', '2014-03-11 02:40:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `datetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=416 ;

--
-- Volcado de datos para la tabla `photos`
--

INSERT INTO `photos` (`id`, `name`, `datetime`, `status`) VALUES
(391, 'img_140214_005344.jpg', '2014-02-12 11:15:12', 1),
(392, 'img_140214_005421.jpg', '2014-02-12 11:15:12', 0),
(393, 'img_140214_005424.jpg', '2014-02-12 11:15:12', 0),
(394, 'img_140214_005426.jpg', '2014-02-12 11:15:12', 0),
(395, 'img_140214_005429.jpg', '2014-02-12 11:15:12', 0),
(396, 'img_140214_005431.jpg', '2014-02-12 11:15:12', 0),
(397, 'img_140214_005433.jpg', '2014-02-12 11:15:12', 0),
(398, 'img_140214_005435.jpg', '2014-02-12 11:15:12', 0),
(399, 'img_140214_005502.jpg', '2014-02-12 11:15:12', 0),
(400, 'img_140214_005504.jpg', '2014-02-12 11:15:12', 0),
(401, 'img_140214_005505.jpg', '2014-02-12 11:15:12', 0),
(402, 'img_140214_005507.jpg', '2014-02-12 11:15:12', 0),
(403, 'img_140214_005509.jpg', '2014-02-12 11:15:12', 0),
(404, 'img_140214_005512.jpg', '2014-02-12 11:15:12', 0),
(405, 'img_140214_005513.jpg', '2014-02-12 11:15:12', 0),
(406, 'img_140214_005515.jpg', '2014-02-12 11:15:12', 0),
(407, 'img_140214_005517.jpg', '2014-02-12 11:15:12', 0),
(408, 'img_140214_005520.jpg', '2014-02-12 11:15:12', 0),
(409, 'img_140214_005523.jpg', '2014-02-12 11:15:12', 0),
(410, 'img_140214_005625.jpg', '2014-02-12 11:15:12', 0),
(411, 'img_140214_005625.jpg', '2014-02-12 11:15:12', 0),
(412, 'img_140214_005628.jpg', '2014-02-12 11:15:12', 0),
(413, 'img_140214_005628.jpg', '2014-02-12 11:15:12', 0),
(414, 'img_140214_005630.jpg', '2014-02-12 11:15:12', 0),
(415, 'img_140214_005630.jpg', '2014-02-12 11:15:12', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan`
--

CREATE TABLE IF NOT EXISTS `plan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `line_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `production_date` date NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_data` int(11) NOT NULL,
  `status_plan` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=82 ;

--
-- Volcado de datos para la tabla `plan`
--

INSERT INTO `plan` (`id`, `model_id`, `user_id`, `line_id`, `shift_id`, `production_date`, `comment`, `quantity`, `created_at`, `updated_at`, `status_data`, `status_plan`) VALUES
(48, 2, 1, 1, 1, '2014-02-20', 'a', 200, '2014-02-20 23:50:56', '2014-02-20 23:50:58', 0, 0),
(49, 2, 1, 1, 1, '2014-02-20', 'aaa', 3000, '2014-02-20 23:54:10', '2014-02-20 23:54:15', 0, 0),
(50, 2, 1, 1, 1, '2014-02-20', 'comentee', 100, '2014-02-21 02:06:38', '2014-02-21 02:07:35', 0, 0),
(51, 1, 1, 1, 1, '2014-02-20', '21', 50000, '2014-02-21 02:08:03', '2014-02-21 02:08:09', 0, 0),
(52, 3, 1, 1, 1, '2014-02-20', '555555555555555555', 2147483647, '2014-02-21 02:08:30', '2014-02-21 02:08:34', 0, 0),
(53, 3, 1, 1, 2, '2014-02-20', '1505', 150, '2014-02-21 02:15:36', '2014-02-21 02:19:21', 0, 0),
(54, 1, 1, 1, 1, '2014-02-20', '111', 111, '2014-02-21 02:16:59', '2014-02-21 02:19:17', 0, 0),
(55, 2, 1, 1, 1, '2014-02-27', '150', 150, '2014-02-21 02:21:48', '2014-02-21 02:21:50', 0, 0),
(56, 2, 1, 1, 1, '2014-02-27', '150', 150, '2014-02-21 02:21:55', '2014-02-21 02:21:58', 0, 0),
(57, 3, 1, 1, 1, '2014-02-27', '555555', 555555, '2014-02-21 02:22:24', '2014-02-21 02:22:27', 0, 0),
(60, 1, 1, 1, 1, '2014-02-25', '5555', 555, '2014-02-21 05:58:34', '2014-02-21 05:58:35', 0, 0),
(61, 2, 1, 1, 1, '2014-02-21', '120', 120, '2014-02-21 23:16:10', '2014-02-21 23:16:15', 0, 0),
(62, 2, 1, 1, 1, '2014-02-21', 'Urgente', 2000, '2014-02-21 23:47:56', '2014-02-21 23:47:59', 0, 0),
(64, 2, 1, 1, 1, '2014-02-28', 'buenos son!', 500, '2014-02-28 23:27:59', '2014-02-28 23:27:59', 1, 0),
(65, 2, 1, 1, 1, '2014-02-28', '1', 2000, '2014-02-28 23:40:13', '2014-02-28 23:40:13', 1, 0),
(66, 2, 1, 1, 1, '2014-02-28', '100', 150, '2014-03-01 00:31:25', '2014-03-01 00:31:25', 1, 1),
(67, 1, 1, 2, 2, '2014-02-28', '120', 120, '2014-03-01 05:50:40', '2014-03-01 05:50:40', 1, 0),
(68, 2, 1, 1, 1, '2014-03-01', '100', 120, '2014-03-02 01:50:02', '2014-03-02 01:50:02', 1, 0),
(69, 1, 1, 1, 1, '0000-00-00', '1', 1, '2014-03-04 00:48:21', '2014-03-04 08:09:30', 1, 1),
(70, 2, 1, 1, 2, '2014-03-03', '120', 120, '2014-03-04 08:14:30', '2014-03-04 08:34:01', 0, 1),
(71, 3, 1, 1, 1, '2014-03-03', 'aaaaaaaaaa', 120, '2014-03-03 08:00:00', '2014-03-04 08:35:03', 1, 2),
(72, 3, 1, 1, 2, '2014-03-03', '120', 120, '2014-03-04 08:20:09', '2014-03-04 08:20:09', 1, 0),
(75, 2, 1, 1, 2, '2014-03-06', '1', 1, '2014-03-07 05:42:04', '2014-03-07 05:42:04', 1, 0),
(78, 8, 1, 2, 1, '2014-03-12', '150', 150, '2014-03-11 06:31:09', '2014-03-11 06:31:09', 1, 0),
(79, 8, 1, 1, 1, '2014-03-10', '150', 150, '2014-03-11 06:31:34', '2014-03-11 06:31:34', 1, 0),
(80, 8, 1, 1, 1, '2014-03-11', '120', 120, '2014-03-12 05:34:50', '2014-03-12 05:34:50', 1, 0),
(81, 8, 1, 2, 1, '2014-03-11', '20', 2200, '2014-03-12 05:35:01', '2014-03-12 05:35:01', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=88 ;

--
-- Volcado de datos para la tabla `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Hello, liquam ultricies malesuada feugiat?', 'Hello, liquam ultricies malesuada feugiat?', 1, '2014-03-10 13:12:13', '0000-00-00 00:00:00'),
(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam rhoncus', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam rhoncus', 13, '2014-03-11 17:25:25', '0000-00-00 00:00:00'),
(3, '', 'yeah baby', 1, '2014-03-12 02:15:44', '2014-03-12 02:15:44'),
(4, '', 'saludios', 1, '2014-03-12 02:30:50', '2014-03-12 02:30:50'),
(5, '', 'Hola mundo', 1, '2014-03-12 02:37:10', '2014-03-12 02:37:10'),
(6, '', 'hola a todos', 1, '2014-03-12 02:39:35', '2014-03-12 02:39:35'),
(7, '', 'dsds', 1, '2014-03-12 02:40:20', '2014-03-12 02:40:20'),
(8, '', 'dsds', 1, '2014-03-12 02:40:36', '2014-03-12 02:40:36'),
(9, '', 'dsds', 1, '2014-03-12 02:40:42', '2014-03-12 02:40:42'),
(10, '', 'aa', 1, '2014-03-12 02:42:25', '2014-03-12 02:42:25'),
(11, '', 'aaaaaaaaaaaaaa', 1, '2014-03-12 02:42:30', '2014-03-12 02:42:30'),
(12, '', 'aaa', 1, '2014-03-12 02:42:55', '2014-03-12 02:42:55'),
(13, '', '123123', 1, '2014-03-12 02:43:01', '2014-03-12 02:43:01'),
(14, '', 'dsds', 1, '2014-03-12 02:43:22', '2014-03-12 02:43:22'),
(15, '', 'cece', 1, '2014-03-12 02:44:50', '2014-03-12 02:44:50'),
(16, '', 'sdds', 1, '2014-03-12 02:59:47', '2014-03-12 02:59:47'),
(17, '', 'dsd', 1, '2014-03-12 03:03:37', '2014-03-12 03:03:37'),
(18, '', 'dsds', 1, '2014-03-12 03:04:05', '2014-03-12 03:04:05'),
(19, '', 'cdcd', 1, '2014-03-12 03:06:53', '2014-03-12 03:06:53'),
(20, '', 'cdcd', 1, '2014-03-12 03:06:56', '2014-03-12 03:06:56'),
(21, '', 'csd', 1, '2014-03-12 03:07:13', '2014-03-12 03:07:13'),
(22, '', 'Hola a todos', 1, '2014-03-12 03:10:25', '2014-03-12 03:10:25'),
(23, '', 'dcsds', 1, '2014-03-12 03:10:38', '2014-03-12 03:10:38'),
(24, '', 'sads', 1, '2014-03-12 03:10:59', '2014-03-12 03:10:59'),
(25, '', 'dcd', 1, '2014-03-12 03:11:43', '2014-03-12 03:11:43'),
(26, '', 'dsds', 1, '2014-03-12 03:12:06', '2014-03-12 03:12:06'),
(27, '', 'dwdw', 1, '2014-03-12 03:12:20', '2014-03-12 03:12:20'),
(28, '', 'dede', 1, '2014-03-12 03:12:29', '2014-03-12 03:12:29'),
(29, '', 'dede', 1, '2014-03-12 03:13:10', '2014-03-12 03:13:10'),
(30, '', 'jo', 1, '2014-03-11 03:14:36', '2014-03-12 03:14:36'),
(31, '', 'cdcd', 1, '2014-03-12 03:15:44', '2014-03-12 03:15:44'),
(32, '', 'frfr', 1, '2014-03-12 03:16:25', '2014-03-12 03:16:25'),
(33, '', 'dsds', 1, '2014-03-12 03:17:54', '2014-03-12 03:17:54'),
(34, '', 'df', 1, '2014-03-12 03:18:13', '2014-03-12 03:18:13'),
(35, '', 'gdf', 1, '2014-03-12 03:18:44', '2014-03-12 03:18:44'),
(36, '', 'dffgf', 1, '2014-03-12 03:19:07', '2014-03-12 03:19:07'),
(37, '', 'ghfghg', 1, '2014-03-12 03:19:17', '2014-03-12 03:19:17'),
(38, '', 'asdas', 1, '2014-03-12 03:19:28', '2014-03-12 03:19:28'),
(39, '', 'dsdsds', 1, '2014-03-12 03:20:25', '2014-03-12 03:20:25'),
(40, '', 'scscscscs', 1, '2014-03-12 03:20:36', '2014-03-12 03:20:36'),
(41, '', 'dsds', 1, '2014-03-12 04:12:54', '2014-03-12 04:12:54'),
(42, '', 'dsds', 1, '2014-03-12 04:13:21', '2014-03-12 04:13:21'),
(43, '', 'dsds', 1, '2014-03-12 04:13:39', '2014-03-12 04:13:39'),
(44, '', 'dsds', 1, '2014-03-12 04:13:52', '2014-03-12 04:13:52'),
(45, '', 'dsds', 1, '2014-03-12 04:14:23', '2014-03-12 04:14:23'),
(46, '', 'dwdw', 1, '2014-03-12 04:14:36', '2014-03-12 04:14:36'),
(47, '', 'w', 1, '2014-03-12 04:14:44', '2014-03-12 04:14:44'),
(48, '', 'sd', 1, '2014-03-12 04:15:49', '2014-03-12 04:15:49'),
(49, '', 'dfd', 1, '2014-03-12 04:16:30', '2014-03-12 04:16:30'),
(50, '', 'yeah baby', 1, '2014-03-12 04:17:02', '2014-03-12 04:17:02'),
(51, '', 'dsds', 1, '2014-03-12 04:22:38', '2014-03-12 04:22:38'),
(52, '', 'dss', 1, '2014-03-12 04:24:41', '2014-03-12 04:24:41'),
(53, '', 'dsds', 1, '2014-03-12 04:25:33', '2014-03-12 04:25:33'),
(54, '', 'cs', 1, '2014-03-12 04:26:33', '2014-03-12 04:26:33'),
(55, '', 'ds', 1, '2014-03-12 04:27:27', '2014-03-12 04:27:27'),
(56, '', 'vgbhn', 1, '2014-03-12 04:29:14', '2014-03-12 04:29:14'),
(57, '', 'de', 1, '2014-03-12 04:30:19', '2014-03-12 04:30:19'),
(58, '', 'de', 1, '2014-03-12 04:30:20', '2014-03-12 04:30:20'),
(59, '', 'ds', 1, '2014-03-12 04:30:37', '2014-03-12 04:30:37'),
(60, '', 'xs', 1, '2014-03-12 04:31:17', '2014-03-12 04:31:17'),
(61, '', 'dssd', 1, '2014-03-12 04:33:34', '2014-03-12 04:33:34'),
(62, '', 'ds', 1, '2014-03-12 04:34:47', '2014-03-12 04:34:47'),
(63, '', 'dedos', 1, '2014-03-12 04:34:57', '2014-03-12 04:34:57'),
(64, '', 'dsds', 1, '2014-03-12 04:35:20', '2014-03-12 04:35:20'),
(65, '', 'sxcx', 1, '2014-03-12 04:36:34', '2014-03-12 04:36:34'),
(66, '', 'aaa', 1, '2014-03-12 04:36:45', '2014-03-12 04:36:45'),
(67, '', 'dsds', 1, '2014-03-12 04:37:13', '2014-03-12 04:37:13'),
(68, '', 'yeah baby', 1, '2014-03-12 04:37:20', '2014-03-12 04:37:20'),
(69, '', 'dsds', 1, '2014-03-12 04:37:50', '2014-03-12 04:37:50'),
(70, '', 'Saludos mis amigos', 1, '2014-03-12 04:38:02', '2014-03-12 04:38:02'),
(71, '', 'Hi!"', 1, '2014-03-12 04:39:47', '2014-03-12 04:39:47'),
(72, '', 'beatuful!', 1, '2014-03-12 04:42:00', '2014-03-12 04:42:00'),
(73, '', 'Saludos', 1, '2014-03-12 05:29:17', '2014-03-12 05:29:17'),
(74, '', 'AJa', 1, '2014-03-12 05:34:01', '2014-03-12 05:34:01'),
(75, '', 'Holas', 13, '2014-03-12 05:35:46', '2014-03-12 05:35:46'),
(76, '', 'A', 13, '2014-03-12 05:36:32', '2014-03-12 05:36:32'),
(77, '', 'aa', 13, '2014-03-12 05:37:41', '2014-03-12 05:37:41'),
(78, '', 'Yeah bab y', 13, '2014-03-12 06:08:35', '2014-03-12 06:08:35'),
(79, '', 'ok', 1, '2014-03-12 06:09:39', '2014-03-12 06:09:39'),
(80, '', 'Hi! every one', 13, '2014-03-12 06:32:53', '2014-03-12 06:32:53'),
(81, '', 'Jeaa', 13, '2014-03-12 06:35:56', '2014-03-12 06:35:56'),
(82, '', 'Hace poquito!', 13, '2014-03-12 06:40:31', '2014-03-12 06:40:31'),
(83, '', 's', 13, '2014-03-12 06:40:42', '2014-03-12 06:40:42'),
(84, '', 'a', 13, '2014-03-11 06:41:08', '2014-03-12 06:41:08'),
(85, '', 'Wow!', 13, '2014-03-12 07:05:25', '2014-03-12 07:05:25'),
(86, '', 'asdasdasdasdasdasdkfhsdkfhjklsdhgfjkhbasdjkfbasdkbfhasdbjhfbsdbfjhsdjfsdjhfjhsdfsdjhfjksdhgfjkhsdjkfgsdjkhfgsdhfjkhsdgfhgasdlkjfgsdgjksdhgfgsdjkfhasdf23asd21f354sd3f54sd56f45sd4', 13, '2014-03-12 07:05:38', '2014-03-12 07:05:38'),
(87, '', 'klsjdnckljsdklcjbdhbcvjkhbbsdjkhbvjkhsdbvhbsdvbhduivbhksbdvjhbsdvhbsjkdhbvjkhsbdvjhbsdhbvsdhbvhbsdvsdfv654sd68v46sd54v65sd4v564sd68v4s6d54v56sd132v4s864v654s86v41d53fvb4z243xcv5zx153v4d64v68d7465v4d86b453d4b864df53b486df4b5df468b4d56s4vb56sx4df68b465zd4b5', 13, '2014-03-12 07:06:48', '2014-03-12 07:06:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productions`
--

CREATE TABLE IF NOT EXISTS `productions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `line_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `arduino_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Volcado de datos para la tabla `productions`
--

INSERT INTO `productions` (`id`, `user_id`, `line_id`, `model_id`, `arduino_id`, `created_at`, `updated_at`) VALUES
(25, 1, 1, 8, 1, '2014-03-11 08:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shifts`
--

CREATE TABLE IF NOT EXISTS `shifts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shi_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `shifts`
--

INSERT INTO `shifts` (`id`, `shi_description`, `created_at`, `updated_at`) VALUES
(1, 'Matutino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Nocturno', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Intermedio', '2014-03-11 03:03:00', '2014-03-11 03:40:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ut_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `user_type`
--

INSERT INTO `user_type` (`id`, `name`, `ut_description`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'Administrator', '2014-03-07 08:00:00', '0000-00-00 00:00:00'),
(2, 'User', 'User', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nick` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usertype_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `photographic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `email`, `nick`, `name`, `password`, `usertype_id`, `active`, `photographic`, `created_at`, `updated_at`) VALUES
(1, 'israel.martinez.vargas@gmail.com', 'isra', 'Israel Martinez', '$2y$10$KabKDCyn6YucSHhlRhyR2eTQeW/N.cbU4NP4VIogSZ/h7DwLH/Ebu', 1, 1, '', '2014-02-15 06:42:53', '2014-03-12 08:00:02'),
(13, 'bertin@dongchuel.co.kr', 'bertin.s', 'Bertin', '$2y$10$9RIhw93iCCwURjUnPp/F7.iEgZF/TGrYCwfdgreLXmSk9VRjGZZYC', 1, 1, '', '2014-03-11 01:31:42', '2014-03-12 06:09:59');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
